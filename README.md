Documentation d'[Ekzamenoj](https://git.framasoft.org/microniko/ekzamenoj).

Pour générer les fichiers HTML

    pandoc importation_csv.tex -t html5 -s  -c img/styles.css -o importation_csv.html

    pandoc fonctionnalites.tex -t html5 -s -c img/styles.css -o fonctionnalites.html 
